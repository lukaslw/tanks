#include "Player.h"

Player::Player() :
	vForMax(100),
	vBackMax(75),
	vFor(0),
	vBack(0),
	vObr(80),
	a(100),
	orgpX(0),
	orgpY(0),
	upP(0),
	downP(0),
	leftP(0),
	rightP(0),
	points(0),
	timePlayerFrame(0),
	playerFrame(0),
	isDead(0),
	id(0),
	side(0)
{
	loadAllTextures();
}

void Player::move(Map &objectMap, map<int, Player> &playerMap, float deltaTime) {

    //�apanie bonus�w
    catchBonus(objectMap);
	
	double pX = playerSprite.getPosition().x;
	double pY = playerSprite.getPosition().y;
	float angle = playerSprite.getRotation();
	Sprite testSprite = playerSprite;

	// zwalnianie / spadek pedu - jezeli nie dajemy gazu
	if (upP != true) {
		if (vFor > 0) {
			vFor -= deltaTime*a;
		}
	}

	// jezeli nie cofamy
	if (downP != true) {
		if (vBack > 0) {
			vBack -= deltaTime*a;
		}
	}

	// poruszenie do przodu
	if (upP == true) {
		if (vFor < vForMax) {
			vFor += deltaTime*a;
		} else {
			vFor = vForMax;
		}
	}
	pX += vFor * deltaTime * sin(angle * PI / 180);
	pY -= vFor * deltaTime * cos(angle * PI / 180);
	
	// poruszenie do tylu
	if (downP == true) {
		if (vBack < vBackMax) {
			vBack += deltaTime*a;
		} else {
			vBack = vBackMax;
		}
	}
	pX -= vBack * deltaTime * sin(angle * PI / 180);
	pY += vBack * deltaTime * cos(angle * PI/ 180);

    if (leftP == true) {
		// obrot w lewo
		angle -= vObr*deltaTime;
		testSprite.setRotation(angle);
    } else if (rightP == true) {
		// obrot w prawo
		angle += vObr*deltaTime;
		testSprite.setRotation(angle);
    }

	// sprawdzanie kolizji:
	testSprite.setPosition((float) pX, (float) pY);
	if (objectMap.collisionTest(testSprite) == false  && playersCollisionTest(playerMap, testSprite) < 0) {
		playerSprite.setPosition((float) pX, (float) pY);
		playerSprite.setRotation(angle);
	} else {
		// przy kolizji nie nadajemy predkosci
		if (vFor > 0) {
			vBack = vFor / 2;
			vFor = 0;
		} else if (vBack > 0) {
			vFor = vBack / 2;
			vBack = 0;
		}
	}
}

bool Player::getShoted() {
	if (isDead == true) {
		playerSprite.setPosition(orgpX, orgpY);
		isDead = false;
		return true;
	}
	return false;
}

// Sprawdzanie kolizji z innymi graczami (TEST)
// Zwraca id przeciwnika z ktorym wystepuje kolizja
// lub -1 jezeli nie ma kolizji
int Player::playersCollisionTest(map<int, Player> &playerMap, Sprite &testSprite) {
	std::map<int, Player>::iterator playerIter;
	for (playerIter = playerMap.begin(); playerIter != playerMap.end(); playerIter++) {
		// Kolizje tylko z przeciwnikami
		if (side != playerIter->second.side) {
			if (BoundingBoxTest(testSprite, playerIter->second.playerSprite) == true) {
			//if(CircleTest(testSprite, spriteVector[i]) == true) {
				//return i;
				return playerIter->second.id;
			}
		}
	}
	return -1; // nie ma kolizji
}

int Player::bulletCollisionTest(map<int, Player> &playerMap, Sprite &bulletSprite) {
	std::map<int, Player>::iterator playerIter;
	// czy dany bullet nie koliduje z zadnym playerem
	for (playerIter = playerMap.begin(); playerIter != playerMap.end(); playerIter++) {
		// sprawdzamy wlasne bullety wiec nie koliduja z nami samymi
		if (playerIter->second.id != id) {
			if (BoundingBoxTest(bulletSprite, playerIter->second.playerSprite) == true) {
				return playerIter->second.id; // id gracza ktory odniosl kolizje
			}
		}
	}
	return -1; // nie ma kolizji
}


Sprite Player::shot() {
	Sprite bullet;
	bullet.setTexture(bulletTextureVector[0]);
	// ustawienie srodka dla sprite (dla poprawnego obracania)
	bullet.setOrigin((float) bulletTextureVector[0].getSize().x / 2, (float) bulletTextureVector[0].getSize().y / 2);
	bullet.setPosition(playerSprite.getPosition().x, playerSprite.getPosition().y);
	bullet.setRotation(playerSprite.getRotation());
	bulletSpriteVector.push_back(bullet);

	// sila odrzutu
	vBack = vBack < vBackMax ? vBack + 50 : vBackMax;

	return bullet;
}

// Przesuniecie wykonywane dla wszystkich bulletow wszystkich playerow
void Player::moveBullets(Map &objectMap, map<int, Player> &playerMap, float deltaTime) {
	Sprite testBullet;
	for (size_t i = 0; i < bulletSpriteVector.size(); i++) {
		float pX = bulletSpriteVector[i].getPosition().x;
		float pY = bulletSpriteVector[i].getPosition().y;
		float angle = bulletSpriteVector[i].getRotation();
		pX += bulletSpeed * deltaTime * sin(angle * PI / 180);
		pY -= bulletSpeed * deltaTime * cos(angle * PI / 180);
		testBullet.setPosition(pX, pY);

		// Kolizja z przeszkoda
		if (objectMap.collisionTest(testBullet) == true) {
			// Usun pocisk
			bulletSpriteVector.erase(bulletSpriteVector.begin() + i);
			return;
		}

		// Kolizja z przeciwnikiem
		int shootedPlayer = bulletCollisionTest(playerMap, testBullet);
		if (shootedPlayer != -1) {
			// Jezeli ktorys pocisk trafil gracza to ustawimy mu to
			playerMap[shootedPlayer].isDead = true;
			playerMap[shootedPlayer].lastKillerID = id;

			// Usun pocisk
			bulletSpriteVector.erase(bulletSpriteVector.begin() + i);
			return;
		}

		// Brak kolizji - ustawiamy w nowym polozeniu
		bulletSpriteVector[i].setPosition(pX, pY);
	}
}

void Player::drawBullets() {
	for (size_t i = 0; i < bulletSpriteVector.size(); i++) {
		window.draw(bulletSpriteVector[i]);
	}
}

void Player::keyPressed(int direction) {
	switch (direction) {
	case upD:
		if (downP == false) {
			upP = true;
		}
		break;
	case downD:
		if (upP == false) {
			downP = true;
		}
		break;
	case leftD:
		if (rightP == false) {
			leftP = true;
		}
		break;
	case rightD:
		if (leftP == false) {
			rightP = true;
		}
		break;
	}
}

void Player::keyReleased(int direction) {
	switch(direction) {
	case upD:
		upP = false;
		break;
	case downD:
		downP = false;
		break;
	case leftD:
		leftP = false;
		break;
	case rightD:
		rightP = false;
		break;
	}
}

void Player::catchBonus(Map &objectMap) {
	/*
    if(objectMap.value(pX/20, pY/20) == '$') {
        objectMap.change(pX/20, pY/20, '0');
        points += 10;
    }
	*/
}

void Player::loadAllTextures() {
	loadTankTextures();
	loadBulletTextures();
}


// TODO
void Player::loadBulletTextures() {
	loadTextures("png/bullet/", bulletTextureVector);
	for (int i = 0; i < amountOfBulletFrames; i++) {
		bulletTextureVector[i].setSmooth(true);
	}
}

// Wczytanie wszystkich tekstur dla czolgu
void Player::loadTankTextures() {
	loadTextures("png/tank/", tankVector);
	// anti-aliasing dla tekstur
	for (int i = 0; i < amountOfFramesPerAnim; i++) {
		tankVector[i].setSmooth(true);
	}
	// ustawienie srodka dla sprite (dla poprawnego obracania)
	playerSprite.setOrigin((float) tankVector[0].getSize().x / 2, (float) tankVector[0].getSize().y / 2);
}

//Dla wyswietlania animacji
void Player::nextFrame(int &frame, int maxFrame) {
    if (frame < maxFrame - 1) {
        frame++;
    } else {
        frame = 0;
    }
}

//Wyswietlenie postaci
void Player::drawPlayer(float deltaTime) {
    timePlayerFrame += deltaTime;
    if(timePlayerFrame > 0.05) {
        timePlayerFrame = 0;
		nextFrame(playerFrame, amountOfFramesPerAnim);
    }
    playerSprite.setTexture(tankVector[playerFrame], 0);
	//TODO!!!
	if(isDead != true) {
		window.draw(playerSprite);
	}
}

// Aktualizowanie widoku ekranu - przesuniecie fragmentu mapy w zaleznosci
// od polozenia gracza na calej mapie
void Player::updateView(Map &objectMap) {
	view = window.getDefaultView();
	float playerX = playerSprite.getPosition().x;
	float playerY = playerSprite.getPosition().y;

	if (playerX > windowXSize / 2) {
		if (playerX < objectMap.windowXMaxSize - windowXSize / 2) {
			viewMoveVector.x = playerX - window.getSize().x / 2;
		} else {
			viewMoveVector.x = windowXSize;
		}
	} else {
		viewMoveVector.x = 0;
	}

	if (playerY > windowYSize / 2) {
		if (playerY < objectMap.windowYMaxSize - windowYSize / 2) {
			viewMoveVector.y = playerY - window.getSize().y / 2;
		} else {
			viewMoveVector.y = windowYSize;
		}
	} else {
		viewMoveVector.y = 0;
	}

	view.move(viewMoveVector);
	window.setView(view);
}