#pragma once
#include "Common.h"
#include "ClientGame.h"
#include "GUI.h"

using namespace sf;

class ClientGame;

class Chat {
private:
	ClientGame *clientObject;

public:
	Chat(ClientGame *clientObj);
	
	bool chatTyping; // czy klient pisze na czacie
	string chatMessage;
	string lastMessage;

	// Aktualna pozycja kursora
	unsigned int cursorPos;

	// Wlacz pisanie na czacie
	void chatTypingOn();

	// Wylacz pisanie na czacie
	void chatTypingOff();

	// Jezeli w trakcie pisania
	bool ifTyping();

	// Pisz wiadomosc
	void writeMessage(Event eventObject);

	// BackSpace
	void backspaceMessage(Event eventObject);

	// Delete
	void deleteMessage(Event eventObject);

	// Wysyla nowa wiadomosc
	void sendMessage();

	// Dodaje nowa wiadomosc na koniec kolejki
	void showMessage(ChatPacket *chatPacket);

	// Wyswietl tresc czatu
	void drawChat();

	// Wyswietl wpisana ostatnio wiadomosc ponownie
	void retypeLastMessage();

	// Poruszanie kursorem - zmiana miejsca wpisywania / edycji tekstu
	void moveCursorLeft();
	void moveCursorRight();

	// Kolejka wiadomosci
	vector<string> chatVector;
};
