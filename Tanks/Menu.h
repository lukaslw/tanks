#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <math.h>
#include <vector>
#include <fstream>
#include <iostream>
#include <sstream>
#include <map>
#include <dirent.h>
#include <string>
#include "Common.h"

using namespace sf;
using namespace std;

extern RenderWindow window;

enum menuOption {
	NoMenu,
	MainMenuActive,
	ClientServerActive,
	HighscoreActive,
	OptionsActive,
	IPSettings,
	NickSettings
};

#pragma WARNING("potrzebne?")
struct score {
    string nick;
    int points;
};

class Common;

class Menu : public Common {
private:
    float mouseX;
    float mouseY;
    int menuSize;
    int menuOptionX;
    int menuOptionY;
    int menuYOffset;
    int menuButtonXSize;
    int menuButtonYSize;
    int backOptionX;
    int backOptionY;
    int backButtonXSize;
    int backButtonYSize;
	int titleXSize;
	int titleYSize;
	int nickXSize;
    Music MusicMenuObject;
	map <string, Texture> textureMap;
	map <string, Sprite> spriteMap;
    vector<score> vectorScores;
	void loadSprite(string textureName, string spriteName, float x, float y);

public:
	int gameType;
	int menuType;
    bool musicActive;
	string ip;
	string nick;
    Menu();
	void menuLoop();
	void loadMenuSprites();
    void loadMusic();
    void stopMusic();
	void drawMainMenu();
    void drawHighscoreMenu();
	void drawOptionsMenu();
	void drawClientServerMenu();
	void drawIPSettings();
	void drawNickSettings();

	void mousePosition();
	void drawCursor();
    void menuClick();
	void clientServerClick();
    void optionsClick();
	void highscoreClick();
    //void menuHighlight();
	String setServerIP();

	// Do zmiany:
    void loadHighscore();
    void sortHighscore();
    void saveHighscore();
	void showHighscorePlayers();
    //void newScore(int points, Font objectFont);
};