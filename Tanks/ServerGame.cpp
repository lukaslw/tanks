#include "ServerGame.h"
#include "Common.h"

int ServerGame::client_id; 

ServerGame::ServerGame() {
    client_id = 0;
	blue_side = 0;
	red_side = 0;

    network = new Server(); 
}

// Aktualizacja danych na serwerze
void ServerGame::update() {
	// Akceptowanie nowego klienta
	if (network->acceptNewClient(client_id)) {
		cout << "Klient o nr " << client_id << " polaczyl sie z serwerem." << endl;

		// Przydzielenie strony po ktorej bdzie gral klient
		int side;
		if (blue_side < red_side) {
			side = BLUE;
			blue_side++;
		}
		else {
			side = RED;
			red_side++;
		}

		// Wyslanie id i strony do klienta
		sendPacketsToClient(IDPacket(client_id, side), client_id);
		client_id++;
	}

	// Odbieranie danych od klientow
	receiveFromClients();
}

// Odbieranie danych od klientow
void ServerGame::receiveFromClients() {
    Packet packet;
	InitPacket *initPacket;
	PlayerPacket *playerPacket;
	BulletPacket *bulletPacket;
	KillPacket *killPacket;
	ChatPacket *chatPacket;

    // Sprawdzamy kolejnych klientow
    map<unsigned int, SOCKET>::iterator iter;

    for (iter = network->sessions.begin(); iter != network->sessions.end(); iter++) {
        
		int data_length = network->receiveData(iter->first, network_data);

        if (data_length <= 0) {
            // nie otrzymano danych
            continue;
        }

        unsigned int i = 0;
        while (i < (unsigned int) data_length) {

			// Pierwszy bit pakietu okresla jego typ
			packet.deserialize(&(network_data[i]));

            switch (packet.packet_type) {
			case INIT_PACKET:
				initPacket = new InitPacket();
				initPacket->deserialize(&(network_data[i]));
				playerInfoMap[initPacket->id] = ServerPlayerInfo(initPacket->nick);
				i += sizeof(InitPacket);
				cout << "Serwer otrzymal pakiet inicjalizujacy od nowego klienta." << endl;
				break;
			case PLAYER_PACKET:
				playerPacket = new PlayerPacket();
				playerPacket->deserialize(&(network_data[i]));
				// Rozeslanie pakietu gracza do innych klientow
				sendPacketsToAll(*playerPacket);
				i += sizeof(PlayerPacket);
				//cout << "Serwer otrzymal pakiet gracza od klienta " << playerPacket.id << endl;
				break;
			case BULLET_PACKET:
				bulletPacket = new BulletPacket();
				bulletPacket->deserialize(&(network_data[i]));
				// Rozeslanie pakietu pocisku do innych klientow
				sendPacketsToAll(*bulletPacket);
				i += sizeof(BulletPacket);
				//cout << "Serwer otrzymal pakiet pocisku od klienta " << bulletPacket.id << endl;
				break;
			case KILL_PACKET:
				killPacket = new KillPacket();
				killPacket->deserialize(&(network_data[i]));
				// Aktualizacja wyniku klienta na serwerze
				updateScore(killPacket->killerID);
				cout << playerInfoMap[killPacket->killerID].nick << " killed " << playerInfoMap[killPacket->victimID].nick << " - " << playerInfoMap[killPacket->killerID].score << " points" << endl;
				i += sizeof(KillPacket);
				break;
			case CHAT_PACKET:
				chatPacket = new ChatPacket();
				chatPacket->deserialize(&(network_data[i]));
				// Wyslanie wiadomosci do wszystkich klientow
				sendPacketsToAll(*chatPacket);
				i += sizeof(ChatPacket);
				break;
            default:
                cout << "Serwer - Bledny typ odebranego pakietu." << endl;
				break;
            }
        }
    }
}

void ServerGame::updateScore(int killerID) {
	playerInfoMap[killerID].score += 10;
}

void ServerGame::serverLoop() {
	Time time = milliseconds(10);
    while (true) {
        update();
		sleep(time);
    }
	terminate();
}