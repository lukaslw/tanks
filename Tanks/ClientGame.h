#pragma once
#include <winsock2.h>
#include <windows.h>
#include <random>
#include "Client.h"
#include "Common.h"
#include "NetworkData.h"
#include "Chat.h"
#include "GUI.h"

using namespace sf;

class Chat;
class GUI;

class ClientGame {

public:
	ClientGame(string ip, string nick);

	int id;
	int side;
	string nick;

	void clientLoop(map<int, Player> &playerMap);

	Client *network;
	char network_data[MAX_PACKET_SIZE];

	Chat *chatObject;
	GUI *guiObject;

	void eventHandler(map<int, Player> &playerMap);

	template <typename PacketType>
	void sendPacket(PacketType &packetObject) {
		const unsigned int packet_size = sizeof(PacketType);
		char packet_data[packet_size];
		packetObject.serialize(packet_data);
		NetworkServices::sendMessage(network->ConnectSocket, packet_data, packet_size);
	}
	void update(map<int, Player> &playerMap);
};
