#include "Chat.h"

Chat::Chat(ClientGame *clientObj) {
	clientObject = clientObj;
	chatMessage = "";
	lastMessage = "";
	chatTyping = false;
	cursorPos = 0;
}

void Chat::chatTypingOn() {
	chatTyping = true;
}

void Chat::chatTypingOff() {
	chatTyping = false;
	chatMessage = "";
	clientObject->guiObject->chatGUI->cursorTime = 0.0; // zresetuj timer kursora
	cursorPos = 0;
}

bool Chat::ifTyping() {
	return chatTyping;
}

void Chat::showMessage(ChatPacket *chatPacket) {
	string chatString = "<";
	chatString += chatPacket->nick;
	chatString += "> ";
	chatString += chatPacket->messageText;
	chatVector.push_back(chatString);
}

void Chat::sendMessage() {
	if (chatMessage != "") {
		clientObject->sendPacket(ChatPacket(chatMessage, clientObject->nick));
		lastMessage = chatMessage;
	}
}

// TODO - Polskie litery
void Chat::writeMessage(Event eventObject) {
	if (eventObject.text.unicode >= 32 && eventObject.text.unicode < 127) {
		chatMessage = chatMessage.substr(0, cursorPos) + (char)eventObject.text.unicode + chatMessage.substr(cursorPos);
		cursorPos++;
	}
}

void Chat::backspaceMessage(Event eventObject) {
	if (chatMessage.size() != 0) {
		if (cursorPos > 0) {
			chatMessage = chatMessage.substr(0, cursorPos - 1) + chatMessage.substr(cursorPos);
			cursorPos--;
		}
	}
}

void Chat::deleteMessage(Event eventObject) {
	if (chatMessage.size() != 0) {
		if (cursorPos < chatMessage.size()) {
			chatMessage = chatMessage.substr(0, cursorPos) + chatMessage.substr(cursorPos + 1);
		}
	}
}

void Chat::retypeLastMessage() {
	chatMessage = lastMessage;
	cursorPos = chatMessage.size();
}

void Chat::moveCursorLeft() {
	if (cursorPos > 0) {
		cursorPos--;
	}
}

void Chat::moveCursorRight() {
	if (cursorPos < chatMessage.size()) {
		cursorPos++;
	}
}