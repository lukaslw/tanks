#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <math.h>
#include <vector>
#include <iostream>
#include <sstream>
#include <dirent.h>
#include "Collision.h"

using namespace sf;
using namespace std;
using namespace Collision;
using namespace std;

// Dla generowania wlasnych warningow:
#define STRINGIZE_HELPER(x) #x
#define STRINGIZE(x) STRINGIZE_HELPER(x)
#define WARNING(desc) message(__FILE__ "(" STRINGIZE(__LINE__) ") : Warning: " #desc)

// Typ okna:
#define windowXSize 800
#define windowYSize 600
#define bitsPerPixel 32

// Typ gry:
enum GameType {
	ClientOnly,
	ServerOnly,
	ClientServer
};

// Strony walczacych
enum Side {
	RED,
	BLUE
};

class Common {
public:
	int loadTextures(string textureDir, vector<Texture> &vectorTexture);
	int loadTextures(string textureDir, map <string, Texture> &textureMap);
};