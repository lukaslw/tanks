#include "Common.h"

// textureDir - sciezka do folderu z teksturami zakonczona znakiem '/'
// vectorTexture - docelowy wektor tekstur
int Common::loadTextures(string textureDir, vector<Texture> &vectorTexture) {
	DIR *dir;
	struct dirent *ent;
	string fullName;
	string filePath;
	string name;
	Texture tempTexture;
	if ((dir = opendir(textureDir.c_str())) != NULL) {
		while ((ent = readdir(dir)) != NULL) {
			// nazwa z rozszerzeniem
			fullName = ent->d_name;
			if(fullName != "." && fullName != "..") {
				// pelna sciezka (folder + nazwa z rozszerzeniem)
				filePath = textureDir + fullName;
				tempTexture.loadFromFile(filePath);
				vectorTexture.push_back(tempTexture);
			}
		}
		closedir (dir);
		return EXIT_SUCCESS;
	} 
	else {
		return EXIT_FAILURE;
	}
}

// textureDir - sciezka do folderu z teksturami zakonczona znakiem '/'
// textureMap - docelowa mapa textur postaci <string, Texture>
int Common::loadTextures(string textureDir, map <string, Texture> &textureMap) {
	DIR *dir;
	struct dirent *ent;
	string fullName;
	string filePath;
	string name;
	Texture tempTexture;
	if ((dir = opendir(textureDir.c_str())) != NULL) {
		while ((ent = readdir(dir)) != NULL) {
			// nazwa z rozszerzeniem
			fullName = ent->d_name;
			if(fullName != "." && fullName != "..") {
				// pelna sciezka (folder + nazwa z rozszerzeniem)
				filePath = textureDir + fullName;
				if (tempTexture.loadFromFile(filePath)) {
					unsigned int dotPosition = fullName.find(".");
					// dlugosc nazwy rozszerzenia z kropka wlacznie
					int fileTypeNameSizeWithDot = fullName.length() - dotPosition;
					// nazwa bez rozszerzenia
					name = fullName.substr(0, fullName.length() - fileTypeNameSizeWithDot);
					textureMap.insert(pair<String, Texture>(name, tempTexture));
				}
			}
		}
		closedir (dir);
		return EXIT_SUCCESS;
	} 
	else {
		return EXIT_FAILURE;
	}
}