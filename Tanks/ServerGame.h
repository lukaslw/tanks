#pragma once
#include <SFML/Graphics.hpp>
#include "Server.h"
#include "Common.h"
#include <thread>

using namespace sf;

class ServerGame {

public:
    ServerGame();
    void update();
	void receiveFromClients();

	template <typename ServerPacket>
	void sendPacketsToAll(ServerPacket &packet) {
		const unsigned int packet_size = sizeof(ServerPacket);
		char packet_data[packet_size];
		packet.serialize(packet_data);
		network->sendToAll(packet_data, packet_size);
	}

	template <typename ServerPacket>
	void sendPacketsToClient(ServerPacket &packet, int clientID) {
		const unsigned int packet_size = sizeof(ServerPacket);
		char packet_data[packet_size];
		packet.serialize(packet_data);
		network->sendToClient(clientID, packet_data, packet_size);
	}

	void serverLoop();
	void updateScore(int killerID);

	// Info o graczu na serwerze
	struct ServerPlayerInfo {
		ServerPlayerInfo() {
			nick = "";
			score = 0;
		}
		ServerPlayerInfo(string playerNick) {
			nick = playerNick;
			score = 0;
		}
		string nick;
		int score;
	};

private:
    // Obecny id klienta do wydania (ew ilosc playerow pomniejszona o 1)
    static int client_id;

	// Ilosc graczy po kazdej ze stron:
	int blue_side;
	int red_side;

	// Kontener informacji o graczach
	map<int, ServerPlayerInfo> playerInfoMap;

    Server *network;

	// bufor danych
	char network_data[MAX_PACKET_SIZE];
};