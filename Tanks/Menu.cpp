#include "Menu.h"

Menu::Menu():
	menuType(MainMenuActive),
	musicActive(false),
	menuSize(4),
	menuYOffset(15),
    menuButtonXSize(88),
    menuButtonYSize(17),
	backButtonXSize(101),
    backButtonYSize(17),
	backOptionY(480),
	titleXSize(200),
	titleYSize(50),
	nickXSize(150)
{
	menuOptionX = windowXSize/2 - menuButtonXSize/2;
    menuOptionY = windowYSize/2 - (menuSize/2)*(menuButtonYSize + menuYOffset);
    backOptionX = windowXSize/2 - backButtonXSize/2;
	ip = "127.0.0.1";
	nick = "Player";
}

// Wczytanie sprite'a
void Menu::loadSprite(string textureName, string spriteName, float x, float y) {
	Sprite tempSprite;
	tempSprite.setTexture(textureMap.find(textureName)->second);
	tempSprite.setPosition(x, y);
	spriteMap.insert(pair<String, Sprite>(spriteName, tempSprite));
}

// Zaladowanie wszystkich sprite'ow
void Menu::loadMenuSprites() {

	loadTextures("png/menu/", textureMap);
	
	loadSprite("background", "background", 0, 0);
	loadSprite("cursor", "cursor", 0, 0);
	
	loadSprite("title", "title", (float) windowXSize/2 - titleXSize/2, 110);
	loadSprite("highscore", "highscoreTitle", (float) windowXSize/2 - menuButtonXSize/2, 100);
	loadSprite("option", "optionTitle", (float) windowXSize/2 - menuButtonXSize/2, 100);
	loadSprite("back", "back", (float) windowXSize/2 - backButtonXSize/2, 480);
	
	// Main menu
	loadSprite("start", "start", (float) menuOptionX, (float) menuOptionY);
	loadSprite("highscore", "highscoreMainMenu", (float) menuOptionX, (float) menuOptionY + 1*(menuButtonYSize + menuYOffset));
	loadSprite("option", "optionMainMenu", (float) menuOptionX, (float) menuOptionY + 2*(menuButtonYSize + menuYOffset));
	loadSprite("quit", "quit", (float) menuOptionX, (float) menuOptionY + 3*(menuButtonYSize + menuYOffset));

	// Options
	loadSprite("music", "music", (float) menuOptionX, (float) menuOptionY + 1*(menuButtonYSize + menuYOffset));
	loadSprite("on", "on", (float) menuOptionX, (float) menuOptionY + 2*(menuButtonYSize + menuYOffset));
	loadSprite("off", "off", (float) menuOptionX, (float) menuOptionY + 3*(menuButtonYSize + menuYOffset));

	// Client/Server
	loadSprite("client", "client", (float) menuOptionX, (float) menuOptionY + 1*(menuButtonYSize + menuYOffset));
	loadSprite("server", "server", (float) menuOptionX, (float) menuOptionY + 2*(menuButtonYSize + menuYOffset));
	loadSprite("both", "both", (float) menuOptionX, (float) menuOptionY + 3*(menuButtonYSize + menuYOffset));

	// IPSettings
	loadSprite("ip", "ip", (float) menuOptionX, (float) menuOptionY);

	// NickSettings
	loadSprite("nick", "nick", (float) windowXSize/2 - nickXSize / 2, (float) menuOptionY);
}

void Menu::loadMusic() {
    if (MusicMenuObject.openFromFile("music_menu.ogg") != false && musicActive == true && menuType != NoMenu) {
        MusicMenuObject.play();
        MusicMenuObject.setLoop(true);
    }
}

void Menu::stopMusic() {
    MusicMenuObject.stop();
}

void Menu::menuLoop() {
	Event EventObject;

	switch(menuType) {
		case MainMenuActive:
			drawMainMenu();
		break;
		case ClientServerActive:
			drawClientServerMenu();
		break;
		case HighscoreActive:
			drawHighscoreMenu();
		break;
		case OptionsActive:
			drawOptionsMenu();
		break;
		case IPSettings:
			drawIPSettings();
		break;
		case NickSettings:
			drawNickSettings();
		break;
	}

	mousePosition();
    drawCursor();
    //menuHighlight();
    window.display();
    window.clear();

	while (window.pollEvent(EventObject)) {
        if(EventObject.type == Event::MouseButtonPressed && EventObject.mouseButton.button == Mouse::Left) {
			switch(menuType) {
				case MainMenuActive:
					menuClick();
				break;
				case ClientServerActive:
					clientServerClick();
				break;
				case OptionsActive:
					optionsClick();
				break;
				case HighscoreActive:
					highscoreClick();
				break;
			}
        }

		// Krzyzyk i ESC - wylaczenie gry
		if(EventObject.type == Event::Closed || (EventObject.type == Event::KeyPressed && EventObject.key.code == Keyboard::Escape)) {
            window.close();
        }
    }
}

void Menu::drawMainMenu() {
	window.draw(spriteMap.find("background")->second);
	window.draw(spriteMap.find("title")->second);
	window.draw(spriteMap.find("start")->second);
	window.draw(spriteMap.find("highscoreMainMenu")->second);
    window.draw(spriteMap.find("optionMainMenu")->second);
	window.draw(spriteMap.find("quit")->second);
}

void Menu::drawClientServerMenu() {
	window.draw(spriteMap.find("background")->second);
	window.draw(spriteMap.find("client")->second);
	window.draw(spriteMap.find("server")->second);
	window.draw(spriteMap.find("both")->second);
	window.draw(spriteMap.find("back")->second);
}

void Menu::drawHighscoreMenu() {
	window.draw(spriteMap.find("background")->second);
	window.draw(spriteMap.find("highscoreTitle")->second);
	window.draw(spriteMap.find("back")->second);
	showHighscorePlayers(); // bedzie zmiana
}

void Menu::drawOptionsMenu() {
	window.draw(spriteMap.find("background")->second);
	window.draw(spriteMap.find("optionTitle")->second);
	window.draw(spriteMap.find("music")->second);
	window.draw(spriteMap.find("on")->second);
	window.draw(spriteMap.find("off")->second);
	window.draw(spriteMap.find("back")->second);
}

void Menu::drawIPSettings() {
	Font fontObject;
	fontObject.loadFromFile("arial.ttf");
	Event EventObject;

	do {
		window.draw(spriteMap.find("background")->second);
		window.draw(spriteMap.find("ip")->second);
		window.draw(spriteMap.find("back")->second);

		window.pollEvent(EventObject);
		if(EventObject.type == Event::KeyPressed) {
			if(ip.size() <= 20) {
				if(EventObject.key.code >= Keyboard::Num0 && EventObject.key.code <= Keyboard::Num9) {
					ip += EventObject.key.code + 22;
				} else if(EventObject.key.code == Keyboard::Period) {
					ip += '.';
				}
			}
			if(EventObject.key.code == Keyboard::BackSpace) {
				if(ip.size() > 0) {
					ip.erase(ip.size()-1);
				}
			}
		}

		Text nickText;
		nickText.setFont(fontObject);
		nickText.setCharacterSize(20);
		nickText.setString(ip);
		nickText.setColor(Color::White);
		nickText.move(window.getSize().x/2 - nickText.getGlobalBounds().width/2, 260);
		window.draw(nickText);

		mousePosition();
		drawCursor();
		window.display();
		window.clear();

	} while(EventObject.key.code != Keyboard::Return && EventObject.key.code != Keyboard::Escape);

	if(EventObject.key.code == Keyboard::Return) {
		//menuType = NoMenu;
		menuType = NickSettings;
	} else {
		menuType = MainMenuActive;
	}
}

void Menu::drawNickSettings() {
	Font fontObject;
	fontObject.loadFromFile("arial.ttf");
	Event EventObject;
	window.pollEvent(EventObject);

	do {
		window.draw(spriteMap.find("background")->second);
		window.draw(spriteMap.find("nick")->second);
		window.draw(spriteMap.find("back")->second);

		window.pollEvent(EventObject);
		if(EventObject.type == Event::KeyPressed) {
            if(nick.size() <= 20) {
                if(EventObject.key.shift == 1) {
                    if(EventObject.key.code >= Keyboard::A && EventObject.key.code <= Keyboard::Z) {
                        nick += EventObject.key.code + 65;
                    }
                }
                else{
                    if(EventObject.key.code >= Keyboard::A && EventObject.key.code <= Keyboard::Z) {
                        nick += EventObject.key.code + 97;
                    }
                }
            }
            if(EventObject.key.code == Keyboard::BackSpace) {
                if(nick.size() > 0) {
                    nick.erase(nick.size()-1);
                }
            }
        }

		Text nickText;
		nickText.setFont(fontObject);
		nickText.setCharacterSize(20);
		nickText.setString(nick);
		nickText.setColor(Color::White);
		nickText.move(window.getSize().x/2 - nickText.getGlobalBounds().width/2, 260);
		window.draw(nickText);

		mousePosition();
		drawCursor();
		window.display();
		window.clear();

	} while(EventObject.type != Event::KeyPressed || EventObject.key.code != Keyboard::Return && EventObject.key.code != Keyboard::Escape);

	if(EventObject.key.code == Keyboard::Return) {
		menuType = NoMenu;
	} else {
		menuType = MainMenuActive;
	}
}

void Menu::menuClick() {
	if(spriteMap.find("start")->second.getGlobalBounds().contains(mouseX, mouseY)) {
		menuType = ClientServerActive;
	}
	else if(spriteMap.find("highscoreMainMenu")->second.getGlobalBounds().contains(mouseX, mouseY)) {
		menuType = HighscoreActive;
	}
	else if(spriteMap.find("optionMainMenu")->second.getGlobalBounds().contains(mouseX, mouseY)) {
		menuType = OptionsActive;
	}
	else if(spriteMap.find("quit")->second.getGlobalBounds().contains(mouseX, mouseY)) {
		window.close();
	}
}

#pragma WARNING("couty")
void Menu::clientServerClick() {
	if(spriteMap.find("client")->second.getGlobalBounds().contains(mouseX, mouseY)) {
		cout << "ClientOnly" << endl;
		gameType = ClientOnly;
		menuType = IPSettings;
	}
	else if(spriteMap.find("server")->second.getGlobalBounds().contains(mouseX, mouseY)) {
		cout << "ServerOnly" << endl;
		gameType = ServerOnly;
		menuType = NoMenu;
	}
	else if(spriteMap.find("both")->second.getGlobalBounds().contains(mouseX, mouseY)) {
		cout << "ClientServer" << endl;
		gameType = ClientServer;
		menuType = IPSettings;
	}
	else if(spriteMap.find("back")->second.getGlobalBounds().contains(mouseX, mouseY)) {
		menuType = MainMenuActive;
	}
}

void Menu::highscoreClick() {
	if(spriteMap.find("back")->second.getGlobalBounds().contains(mouseX, mouseY)) {
		menuType = MainMenuActive;
	}
}

void Menu::optionsClick() {
	if(spriteMap.find("on")->second.getGlobalBounds().contains(mouseX, mouseY)) {
		MusicMenuObject.setVolume(100);
        musicActive = true;
	}
	else if(spriteMap.find("off")->second.getGlobalBounds().contains(mouseX, mouseY)) {
		MusicMenuObject.setVolume(0);
        musicActive = false;
	}
	else if(spriteMap.find("back")->second.getGlobalBounds().contains(mouseX, mouseY)) {
		menuType = MainMenuActive;
	}
}

void Menu::mousePosition() {
    Vector2i mousePostion = Mouse::getPosition(window);
    mouseX = (float) mousePostion.x;
    mouseY = (float) mousePostion.y;
}

void Menu::drawCursor() {
	spriteMap.find("cursor")->second.setPosition(mouseX, mouseY);
    window.draw(spriteMap.find("cursor")->second);
}

/*
void Menu::menuHighlight() {

}
*/

#pragma WARNING("Do bazy danych:")

void Menu::showHighscorePlayers() {
	Font fontObject;
    string text;
    int size;

	fontObject.loadFromFile("arial.ttf");

    vectorScores.size() > 9 ? size = 9 : size = vectorScores.size();

    for(int i = 0; i < size; i++) {
        const char *nick = vectorScores[i].nick.c_str();
        int points = vectorScores[i].points;
        stringstream stringS1 (stringstream::in | stringstream::out);
        stringS1 << i+1;
        stringstream stringS2 (stringstream::in | stringstream::out);
        stringS2 << points;
        string player = stringS1.str() + ". " + nick + " - " + stringS2.str();
        Text playerText;
        playerText.setString(player);
        playerText.setColor(Color::White);
        playerText.setFont(fontObject);
        playerText.setCharacterSize(20);
        playerText.move((float) window.getSize().x/2-70, (float) 200+i*20);
        window.draw(playerText);
    }
}

//Sortowanie wynikow
void Menu::sortHighscore() {
    for(size_t x = 0; x < vectorScores.size(); x++) {
        for(size_t y = 0; y < vectorScores.size()-1; y++) {
            if(vectorScores[y].points < vectorScores[y+1].points) {
                score temp = vectorScores[y+1];
                vectorScores[y+1] = vectorScores[y];
                vectorScores[y] = temp;
            }
        }
    }
}

//Wczytanie listy highscore z pliku do wektora
void Menu::loadHighscore() {
    fstream file;
    string nick;
    string points;
    score player;
    string filename = "HighScore.txt";
    file.open(filename.c_str(), ios::in);
    if(file.good() == true) {
        while(file.eof() != true) {
            getline(file, nick);
            getline(file, points);
            player.nick = nick;
            player.points = atoi(points.c_str()); //konwersja na int
            vectorScores.push_back(player);
        }
        sortHighscore();
        file.close();
    }
}

/*
//Dodanie nowego wyniku
void Menu::newScore(int points, Font objectFont) {
    Event EventObject;
    string nick;

    do {
        //background
        Sprite background;
        background.setTexture(backgroundTexture, 0);
        window.draw(background);

        //"Game Over"
        Sprite gameover;
        gameover.setTexture(gameoverTexture, 0);
        gameover.setPosition(window.getSize().x/2 - gameover.getGlobalBounds().width/2, 180);
        window.draw(gameover);

        //"Your name:"
        Sprite yourname;
        yourname.setTexture(yournameTexture, 0);
        yourname.setPosition(window.getSize().x/2 - yourname.getGlobalBounds().width/2, 230);
        window.draw(yourname);

        window.pollEvent(EventObject);
        if(EventObject.type == Event::KeyPressed) {
            if(nick.size() <= 20) {
                if(EventObject.key.shift == 1) {
                    if(EventObject.key.code >= Keyboard::A && EventObject.key.code <= Keyboard::Z) {
                        nick += EventObject.key.code + 65;
                    }
                }
                else{
                    if(EventObject.key.code >= Keyboard::A && EventObject.key.code <= Keyboard::Z) {
                        nick += EventObject.key.code + 97;
                    }
                }
            }
            if(EventObject.key.code == Keyboard::BackSpace) {
                if(nick.size() > 0) {
                    nick.erase(nick.size()-1);
                }
            }
        }
        Text nickText;
        nickText.setFont(objectFont);
        nickText.setCharacterSize(20);
        nickText.setString(nick);
        nickText.setColor(Color::White);
        nickText.move(window.getSize().x/2 - nickText.getGlobalBounds().width/2, 260);
        window.draw(nickText);
        window.display();
        window.clear();
    } while(EventObject.key.code != Keyboard::Return && EventObject.key.code != Keyboard::Escape);

    if(EventObject.key.code == Keyboard::Return) {
        score newscore;
        newscore.nick = nick;
        newscore.points = points;
        vectorScores.push_back(newscore);
        sortHighscore();
        saveHighscore();
    }
}
*/

//Zapisz liste highscore z wektora do pliku
void Menu::saveHighscore() {
    fstream plik;
    score player;
    string nick;
    char score[128];
    string filename = "HighScore.txt";
    plik.open(filename.c_str(), ios_base::out);
    if(plik.good() == true) {
        int size;
        vectorScores.size() > 9 ? size = 9 : size = vectorScores.size();
        for(int i = 0; i < size; i++) {
            nick = vectorScores[i].nick;
            sprintf_s(score, "%d", vectorScores[i].points); //konwersja na char*
            plik << nick << '\n';
            plik << score;
            if(i != size - 1) {
                plik << '\n';
            }
        }
        plik.flush();
        plik.close();
    }
}