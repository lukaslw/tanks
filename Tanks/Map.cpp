#include "Map.h"

char **Map::Allocate2DArray(int nRows, int nCols) {
    //(step 1) allocate memory for array of elements of column
    char **ppi = new char*[nRows];

    //(step 2) allocate memory for array of elements of each row
    char *curPtr = new char [nRows * nCols];

    // Now point the pointers in the right place
    for (int i = 0; i < nRows; ++i) {
        *(ppi + i) = curPtr;
         curPtr += nCols;
    }
    return ppi;
}

void Map::Free2DArray(char** Array) {
    delete [] *Array;
    delete [] Array;
}

Map::Map(int x_size, int y_size) {
	xSize = x_size;
	ySize = y_size;
	windowXMaxSize = x_size * 20;
	windowYMaxSize = y_size * 20;
    this -> map = Allocate2DArray(x_size, y_size);

	#pragma WARNING("jakies shity")
    timeCoinFrame = 0;
	timePortalFrame=0;
    coinFrame = 0;
	portalFrame=0;
}

Map::~Map() {
	Free2DArray(map);
}

//Wczytanie tekstur dla rozgrywki
void Map::loadAllTextures() {
	loadTextures("png/background/", backgroundVector);
	loadTextures("png/ground/", groundVector);
}

// Wczytanie spritow gry
void Map::loadSprites() {
	// czyszczenie wektora (dla kolejnego wczytania)
	// i nowej planszy:
	spriteVector.clear();

	Sprite ground;
	ground.setTexture(groundVector[0], 0);

	Sprite ground2;	
	ground2.setTexture(groundVector[1], 0);

	Sprite ground3;
	ground3.setTexture(groundVector[2], 0);

	for (int y = 0; y < ySize; y++) {
        for (int x = 0; x < xSize; x++) {
            if (map[x][y] == 'X') {
				ground.setPosition((float) x*20, (float) y*20);
				spriteVector.push_back(ground);
            }
			else if (map[x][y] == 'G') {
				ground2.setPosition((float) x*20, (float) y*20);
				spriteVector.push_back(ground2);
            }
            else if (map[x][y] == 'Z') {
				ground3.setPosition((float) x*20, (float) y*20);
				spriteVector.push_back(ground3);
            }
        }
    }
}

//Wyswietlenie mapy
void Map::drawMap() {
	for (size_t i = 0; i < spriteVector.size(); i++) {
		window.draw(spriteVector[i]);
	}
}

// Sprawdzanie kolizji ze spritami na mapie
bool Map::collisionTest(Sprite &testSprite) {
	for (size_t i = 0; i < spriteVector.size(); i++) {
		if (BoundingBoxTest(testSprite, spriteVector[i]) == true) {
		//if(CircleTest(testSprite, spriteVector[i]) == true) {
			return true;
		}
	}
	return false;
}

/*
//Wyswietlanie licznika czasu
void Map::drawTimer(float gameTime) {
	//do poprawy!
	Font objectFont.loadfromfile(""); !
    stringstream stringS (stringstream::in | stringstream::out);
    int tempTime = gameTime * 10;
    gameTime = tempTime * 0.1;
    stringS << gameTime;
    string Timer = "Czas gry: " + stringS.str();
    if(tempTime%10==0) Timer += ".0";
    Text timerText;
    timerText.setFont(objectFont);
    timerText.setCharacterSize(20);
    timerText.setString(Timer);
    timerText.setColor(Color::White);
    timerText.move(350, 50);
    window.draw(timerText);
}
*/

//Wyswietlenie tla
void Map::drawBackground(int &levelActive) {
    Sprite background;
	background.setTexture(backgroundVector[levelActive - 1]);
	window.draw(background);
}

/*
//Wyswietlenie ilosci punktow
void Map::drawPoints(Player &objectPlayer, Font objectFont) {
    stringstream stringS (stringstream::in | stringstream::out);
    stringS << objectPlayer.points;
    string Timer = "Points: " + stringS.str();
    Text pointsText;
    pointsText.setFont(objectFont);
    pointsText.setCharacterSize(20);
    pointsText.setString(Timer);
    pointsText.setColor(Color::White);
    pointsText.move(50, 50);
    window.draw(pointsText);
}
*/

/*
//Wyswietlenie ilosci �y�
void Map::drawLife(Player &objectPlayer) {
    Sprite hp;
    hp.setTexture(hpTexture, 0);
    for(int i = 0; i < objectPlayer.life; i++) {
        hp.setPosition((float) 650 + 40*i, (float) 50);
        window.draw(hp);
    }
}
*/

//Wyswietlenie klatki
void Map::drawScene(int &levelActive, float gameTime, float deltaTime) {
    drawBackground(levelActive);
    drawMap();
    //drawTimer(gameTime, ArialFont);
    //drawLife(objectChar);
    //drawPoints(objectChar, ArialFont);
}

// Pozycja poczatkowa gracza
void Map::loadPosition(Player &objectPlayer, int playerSide) {
	char letter;
	switch (playerSide) {
		case RED:
			objectPlayer.playerSprite.setRotation(180);
			letter = 'R';
		break;
		case BLUE:
			letter = 'B';
		break;
	}

	for(int y = 0; y < ySize; y++) {
        for(int x = 0; x < xSize; x++) {
            if(map[x][y] == letter) {
				objectPlayer.playerSprite.setPosition((float) x*20, (float) y*20);
				objectPlayer.orgpX = objectPlayer.playerSprite.getPosition().x;
				objectPlayer.orgpY = objectPlayer.playerSprite.getPosition().y;
				map[x][y] = '0';
				return;
            }
        }
    }
}

//Wczytanie poziomu do tablicy z pliku
void Map::loadLevel(int &levelActive) {
    FILE *levelFile;
    errno_t error = 0;

    switch(levelActive) {
        case 1:
        error = fopen_s(&levelFile, "1.level", "rb");
        break;
        case 2:
        error = fopen_s(&levelFile, "2.level", "rb");
        break;
        case 3:
        error = fopen_s(&levelFile, "3.level", "rb");
        break;
        case 4:
        error = fopen_s(&levelFile, "4.level", "rb");
        break;
    }

    if(!error) {
        for(int y = 0; y < ySize; y++) {
	        for(int x = 0; x < xSize + 2; x++) {
		        char letter = getc(levelFile);
		        if(letter != '\n' && letter != '\r') {
                    map[x][y] = letter;
		        }
	        }
        }
    }
}

//Dla wyswietlania animacji
void Map::nextFrame(int &frame, int amount) {
    if(frame < amount - 1) {
        frame++;
    }
    else {
        frame = 0;
    }
}

/*
//Wyswietlenie coinsow
void Map::drawCoin(int x, int y, float deltaTime) {
    Sprite coin;
    timeCoinFrame += deltaTime;
    if(timeCoinFrame > 0.5) {
        timeCoinFrame = 0;
        nextFrame(coinFrame, 6);
    }
    coin.setTexture(coinsLoad[coinFrame]);
    coin.setPosition((float) x*20, (float) y*20);
    window.draw(coin);
}
*/