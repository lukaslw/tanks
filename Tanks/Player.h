#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <math.h>
#include <vector>
#include <iostream>
#include <sstream>
#include "Map.h"
#include "Collision.h"
#include "Common.h"

#define PI 3.14159265
#define eps 0.0000001

using namespace sf;
using namespace std;
using namespace Collision;

enum Direction {
	upD,
	downD,
	leftD,
	rightD
};

class Common;

class Player : public Common {
private:
    friend class Map;
	bool upP;
	bool downP;
    bool leftP;
    bool rightP;

	const static int amountOfFramesPerAnim = 5;

	// obliczanie nr nastepnej klatki animacji
	void nextFrame(int &frame, int maxFrame);

	// wczytanie wszystkich tekstur czolgu
	void loadTankTextures();
	// tekstury pocisku
	void loadBulletTextures();

	// przesuniecie widoku gry
	View view;
	Vector2f viewMoveVector;

public:
    Player();
	
	// glowny sprite
	Sprite playerSprite;
	// wektory tekstur czolgu
	vector<Texture> tankVector;
	// pociski
	const static int amountOfBulletFrames = 1;
	const static int bulletSpeed = 500;
	vector<Texture> bulletTextureVector;
	vector<Sprite> bulletSpriteVector;

	int id; // ktory player
	int side; // strona po ktorej walczy
	bool isDead; //nie zyje
	int lastKillerID; // kto nas ostatnio zabil
    float orgpX, orgpY; //polozenie poczatkowe
    double vFor; //predkosc do przodu
	double vBack; //predkosc do tylu
	double vForMax; //predkosc maksymalna do przodu
	double vBackMax; // predkosc maksymalna do tylu
	double timeOfMovement; //czas ruchu od startu
	float vObr; // predkosc obracania
    double a; //przyspieszenie
    int points; //punkty

	void move(Map &objectMap, map<int, Player> &playerMap, float deltaTime); //poruszenie
	void moveBullets(Map &objectMap, map<int, Player> &playerMap, float deltaTime);
	Sprite shot(); //strzal
	bool getShoted(); // czy player zostal postrzelony i resetujemy pozycje
	int playersCollisionTest(map<int, Player> &playerMap, Sprite &testSprite);
	int bulletCollisionTest(map<int, Player> &playerMap, Sprite &bulletSprite);
    
	void keyPressed(int direction);
	void keyReleased(int direction);

	void catchBonus(Map &objectMap); //zbieranie bonus�w
    void drawPoints(Font ArialFont);
	
	//wczytanie wszystkich tekstur
	void loadAllTextures();

    int playerFrame;
    float timePlayerFrame;
    void drawPlayer(float deltaTime);
	void drawBullets();
	void updateView(Map &objectMap);
};