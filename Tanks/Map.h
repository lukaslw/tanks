#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <math.h>
#include <vector>
#include <iostream>
#include <sstream>
#include "Collision.h"
#include "Player.h"
#include "Common.h"

using namespace sf;
using namespace std;
using namespace Collision;

extern RenderWindow window;

class Char;
class Enemy;
class Player;

class Map : public Common {
private:
	char **map;
	static const int amountOfBackgrounds = 3;
	static const int amountOfGrounds = 3;
	float timeCoinFrame;
	float timePortalFrame;
    int coinFrame;
	int portalFrame;

	vector<Texture> backgroundVector;
	vector<Texture> groundVector;
	vector<Sprite> spriteVector;

    char **Allocate2DArray(int nRows, int nCols);
    void Free2DArray(char** Array);
    void drawPlayer(float x, float y);
    //void drawTimer(float gameTime);
    void drawBackground(int &levelActive);
    //void drawPoints(Player &objectPlayer, Font objectFont);
    //void drawLife(Player &objectPlayer);
    void drawMap();

public:
    Map(int, int);
    ~Map();

	int xSize, ySize; // wymiary mapy (w kratkach)
	int windowXMaxSize, windowYMaxSize; // wymiary mapy (w pikselach)

    void drawScene(int &levelActive, float gameTime, float deltaTime);
	bool collisionTest(Sprite &testSprite);
    void loadLevel(int &levelActive);
	void loadAllTextures();
	void loadSprites();
	void loadPosition(Player &objectPlayer, int playerSide);
    void nextFrame(int &frame, int amount);
    void change(int x, int y, char letter) {
        map[x][y] = letter;
    }
    char value(int x, int y) {
        return map[x][y];
    }
};
