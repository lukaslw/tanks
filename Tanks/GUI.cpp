#include "GUI.h"

GUI::GUI(ClientGame *clientObj) {
	clientObject = clientObj;
	chatGUI = new ChatGUI(clientObj);
	chatGUI->createChatBox();
}

ChatGUI::ChatGUI(ClientGame *clientObj) {
	clientObject = clientObj;

	chatFont.loadFromFile("arial.ttf");

	chatLine.setFont(chatFont);
	chatLine.setCharacterSize(fontSize); // w pixelach
	chatLine.setColor(sf::Color::White);

	displayedMessage = "";
}

void ChatGUI::createChatBox() {
	Texture chatTexture;
	chatTexture.create((unsigned int)chatXSize, (unsigned int)chatYSize);
	chatSprite.setColor(sf::Color(0, 0, 0, 128)); // transparentny czarny - temp
	chatSprite.setTexture(chatTexture);
	chatSprite.setPosition(sf::Vector2f(chatXPos, chatYPos));

	Texture inputFieldChatTexture;
	inputFieldChatTexture.create((unsigned int)chatXSize, (unsigned int)fontSize + lineDiffY);
	inputFieldChatSprite.setColor(sf::Color(0, 0, 0, 128)); // transparentny czarny - temp
	inputFieldChatSprite.setTexture(inputFieldChatTexture);
	inputFieldChatSprite.setPosition(sf::Vector2f(chatXPos, chatYPos + amountOfLines * (fontSize + lineDiffY)));
}

void ChatGUI::showChatBox() {
	View view = window.getView();
	float windowX = view.getCenter().x - windowXSize / 2;
	float windowY = view.getCenter().y - windowYSize / 2;
	chatSprite.setPosition(Vector2f(chatXPos + windowX, chatYPos + windowY));
	inputFieldChatSprite.setPosition(Vector2f(chatXPos + windowX, chatYPos + amountOfLines * (fontSize + lineDiffY) + windowY));
	window.draw(chatSprite);
	window.draw(inputFieldChatSprite);
}

void ChatGUI::showChatText() {
	View view = window.getView();
	float windowX = view.getCenter().x - windowXSize / 2;
	float windowY = view.getCenter().y - windowYSize / 2;
	vector<string> &chatVector = clientObject->chatObject->chatVector;

	if (chatVector.size() == 0) {
		return;
	}

	vector<string> lines;
	int lineIndex = chatVector.size() - amountOfLines;
	if (lineIndex < 0) {
		lineIndex = 0;
	}
	int lineSize = chatVector.size();
	if (lineSize > amountOfLines) {
		lineSize = amountOfLines;
	}
	for (int i = 0; i < lineSize; i++) {
		chatLine.setString(chatVector[lineIndex + i]);
		clientObject->guiObject->createLines(&chatLine, chatXSize - 2 * lineDiffX, lines);
	}

	int size = lines.size();
	if (size > amountOfLines) {
		size = amountOfLines;
	}
	int index = lines.size() - size;
	for (int i = 0; i < size; i++) {
		chatLine.setString(lines[index]);
		chatLine.setPosition(Vector2f(chatXPos + windowX + lineDiffX, chatYPos + windowY + (fontSize + lineDiffY) * i));
		index++;
		window.draw(chatLine);
	}
}

void GUI::createLines(Text *text, float windowWidth, vector<string> &linesVector) {
	string tempString;
	string string = text->getString();
	Text tempText;
	tempText.setFont(chatGUI->chatFont);
	tempText.setCharacterSize(chatGUI->fontSize);
	while (1) {
		tempText.setString(string);
		if (getTextWidth(&tempText) <= windowWidth) {
			linesVector.push_back(string);
			return;
		}
		for (size_t i = 0; i < string.size(); i++) {
			tempString = "";
			tempString.append(string, 0, i);
			tempText.setString(tempString);
			float lineWidth = getTextWidth(&tempText);
			if (lineWidth >= windowWidth) {
				linesVector.push_back(tempString);
				tempString = "";
				tempString.append(string, i, string.size());
				string = tempString;
				break;
			}
		}
	}
}

float GUI::getTextWidth(Text *text) {
	if (text->getString().getSize() == 0) {
		return 0;
	}

	float textWidth = text->getLocalBounds().width;
	int spaceWidth = chatGUI->chatFont.getGlyph(0x20, chatGUI->fontSize, false).advance;	

	string string = text->getString();
	size_t i;
	// Sprawdzamy od poczatku
	for (i = 0; i < string.size(); i++) {
		if (string.at(i) == ' ') {
			textWidth += spaceWidth;
		} else {
			break;
		}
	}
	// Tylko spacje - nie sprawdzamy juz od konca
	if (i == string.size()) {
		return textWidth;
	}
	// Sprawdzamy od konca
	for (i = string.size() - 1; i > 0; i--) {
		if (string.at(i) == ' ') {
			textWidth += spaceWidth;
		} else {
			break;
		}
	}

	return textWidth;
}

string GUI::cutStringFromLeft(Text *text, float windowWidth) {
	string textString = text->getString();
	if (textString.size() == 0) {
		return "";
	}
	string tempString;
	float messageWidth = clientObject->guiObject->getTextWidth(text);
	if (messageWidth < windowWidth) {
		return textString;
	}
	Text tempText;
	tempText.setFont(*text->getFont());
	tempText.setCharacterSize(text->getCharacterSize());
	for (size_t i = 0; i < textString.size(); i++) {
		tempString = "";
		tempString.append(textString, i, textString.size());
		tempText.setString(tempString);
		messageWidth = clientObject->guiObject->getTextWidth(&tempText);
		if (messageWidth < windowWidth) {
			return tempString;
		}
	}
	return "";
}

void ChatGUI::showClientMessage(float deltaTime) {
	View view = window.getView();
	float windowX = view.getCenter().x - windowXSize / 2;
	float windowY = view.getCenter().y - windowYSize / 2;

	// Wyswietlanie tekstu wpisywanego przez uzytkownika
	Text message;
	message.setFont(chatFont);
	message.setCharacterSize(fontSize);
	message.setString(clientObject->chatObject->chatMessage);
	displayedMessage = clientObject->guiObject->cutStringFromLeft(&message, chatXSize - 2 * lineDiffX);
	message.setString(displayedMessage);
	message.setColor(Color::White);
	message.setPosition(Vector2f(chatXPos + windowX + lineDiffX, chatYPos + windowY + (fontSize + lineDiffY) * amountOfLines));
	window.draw(message);

	int currCursor = clientObject->chatObject->cursorPos;
	int textSize = clientObject->chatObject->chatMessage.size();
	if (currCursor < lastCursorPos) {
		if (textSize == lastTextSize || displayedMessage.size() < textSize) {
			if (cursorGUIPos > 0) {
				cursorGUIPos--;
			}
		} else {
			cursorGUIPos = displayedMessage.size();
		}
	} else if (currCursor > lastCursorPos) {
		if (cursorGUIPos < currCursor) {
			if (cursorGUIPos < displayedMessage.size()) {
				if (displayedMessage.size() == textSize || cursorGUIPos == displayedMessage.size() - 1 || textSize == lastTextSize) {
					cursorGUIPos++;
				}
			}
		}
	}
	lastCursorPos = currCursor;
	lastTextSize = textSize;

	// Migajacy kursor w trakcie pisania
	if (clientObject->chatObject->ifTyping()) {
		if (cursorTimer(deltaTime)) {
			Text tempMessage;
			string tempMessageString;
			tempMessage.setFont(chatFont);
			tempMessage.setCharacterSize(fontSize);
			//tempMessageString.append(displayedMessage, 0, clientObject->chatObject->cursorPos);
			tempMessageString.append(displayedMessage, 0, cursorGUIPos);
			//cout << cursorGUIPos << clientObject->chatObject->cursorPos << endl;
			tempMessage.setString(tempMessageString);

			Text textCursor;
			float textCursorX = chatXPos + windowX + lineDiffX + clientObject->guiObject->getTextWidth(&tempMessage) - cursorXLeftDiff;

			textCursor.setFont(chatFont);
			textCursor.setCharacterSize(fontSize);
			message.setString("|");
			message.setColor(Color::White);
			message.setPosition(Vector2f(textCursorX, chatYPos + windowY + (fontSize + lineDiffY) * amountOfLines));
			window.draw(message);
		}
	}
}

bool ChatGUI::cursorTimer(float deltaTime) {
	cursorTime += deltaTime;
	if (cursorTime >= 2 * tickTime) {
		cursorTime = 0.0;
	}
	if (cursorTime < tickTime) {
		return true;
	} else {
		return false;
	}
}

void GUI::drawGUI(float deltaTime) {
	chatGUI->showChatBox();
	chatGUI->showChatText();
	chatGUI->showClientMessage(deltaTime);
}