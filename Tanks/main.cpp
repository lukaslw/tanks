﻿#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <math.h>
#include <vector>
#include <iostream>
#include <sstream>
#include <process.h>
#include <thread>

#include "Player.h"
#include "Menu.h"
#include "Map.h"
#include "ServerGame.h"
#include "ClientGame.h"
#include "GUI.h"

using namespace sf;
using namespace std;

RenderWindow window;

int main() {
	int levelActive = 1;
	std::map<int, Player> playerMap;
	std::map<int, Player>::iterator playerIter;
    Map MapObject(80,60);
    Time GlobalTime;
    Clock GlobalClock;
    Menu MenuObject;
    Music MusicGameObject;
	ServerGame *ServerObject;
	ClientGame *ClientObject;
	thread *threadServer;
	thread *threadClient;
    float gameTime, deltaTime, lastTime;

    // Utworz okno
	window.create(VideoMode(windowXSize, windowYSize, bitsPerPixel), "Tanks - Przetwarzanie Rozproszone 2014");
    window.setMouseCursorVisible(0);

	// Ladowanie tekstur dla menu
	MenuObject.loadMenuSprites();

	// Zaladowanie najlepszych wynikow z pliku - TODO
    MenuObject.loadHighscore();

	// Petla menu
	while (MenuObject.menuType != NoMenu && window.isOpen()) {
		MenuObject.menuLoop();
    }

	// Serwer uruchamiany w watku glownym aplikacji
	if (MenuObject.gameType == ServerOnly) {
		ServerObject = new ServerGame();
		window.close();
		ServerObject->serverLoop();
		return 0;
	}
	// Serwer uruchamiany w odrebnym watku - threadServer
	else if (MenuObject.gameType == ClientServer) {
		ServerObject = new ServerGame();
		threadServer = new thread(&ServerGame::serverLoop, ServerObject);
	}

	if (MenuObject.gameType == ClientOnly || MenuObject.gameType == ClientServer) {
		// Wczytanie pliku z plansza
		MapObject.loadLevel(levelActive);

		// Wczytanie tekstur klienta
		// TODO - pasek ladowania tutaj
		MapObject.loadAllTextures();
		MapObject.loadSprites();

		ClientObject = new ClientGame(MenuObject.ip, MenuObject.nick);

		// Watek klienta (aktualizujacy dane i wysylajacy wlasne polozenie, itp.)
		threadClient = new thread(&ClientGame::clientLoop, ClientObject, ref(playerMap));
	}

    // Poczatek mierzenia czasu
    GlobalTime = GlobalClock.getElapsedTime();
    lastTime = GlobalTime.asSeconds();

	// Oczekiwanie na podlaczenie do serwera i uzyskanie ID
	// TODO - pasek oczekiwania na polaczenie tutaj
	while (ClientObject->id == -1) {
		sleep(milliseconds(10));
	}

	// Wyslanie do serwera pakietu inicjalizujacego z informacjami o kliencie np. nickiem
	ClientObject->sendPacket(InitPacket(ClientObject->id, MenuObject.nick));

	// Ustawienie gracza na odpowiednim spawn-poincie
	MapObject.loadPosition(playerMap[ClientObject->id], playerMap[ClientObject->id].side);

    // Petla glowna
    while (window.isOpen()) {
		// Pomiar czasu
        GlobalTime = GlobalClock.getElapsedTime();
        gameTime = GlobalTime.asSeconds();
        deltaTime = gameTime - lastTime;
        lastTime = gameTime;

		// Czy gracz zostal postrzelony
		// jezeli tak resetujemy polozenie i wysylamy info na serwer
		if (playerMap[ClientObject->id].getShoted()) {
			ClientObject->sendPacket(KillPacket(ClientObject->id, playerMap[ClientObject->id].lastKillerID));
		};

		// Poruszanie sie gracza
		playerMap[ClientObject->id].move(MapObject, playerMap, deltaTime);

		// Poruszanie sie pociskow
		for (playerIter = playerMap.begin(); playerIter != playerMap.end(); playerIter++) {
			playerIter->second.moveBullets(MapObject, playerMap, deltaTime);
		}
		
		// Aktualizacja widoku
		playerMap[ClientObject->id].updateView(MapObject);
		
		// Wyswietlenie mapy, pociskow, graczy
		MapObject.drawScene(levelActive, gameTime, deltaTime);
		for (playerIter = playerMap.begin(); playerIter != playerMap.end(); playerIter++) {
			playerIter->second.drawBullets();
			playerIter->second.drawPlayer(deltaTime);
		}

		// Wyswietlenie GUI
		ClientObject->guiObject->drawGUI(deltaTime);

		// Wyswietlenie sceny
		window.display();
		window.clear();

		// Obsluga klawiatury
		ClientObject->eventHandler(playerMap);
    }
    return 0;
} 