#pragma once
#include <winsock2.h>
#include <windows.h>
#include "NetworkData.h"

class NetworkServices {
public:
	static int sendMessage(SOCKET curSocket, char * message, int messageSize);
	static int receiveMessage(SOCKET curSocket, char * buffer, int bufSize);
};