#pragma once
#include "ClientGame.h"
#include "Common.h"

using namespace sf;

class ClientGame;

extern RenderWindow window;

class ChatGUI;

class GUI {
public:
	GUI(ClientGame *clientObj);
	ClientGame *clientObject;

	float windowX;
	float windowY;

	ChatGUI *chatGUI;
	void drawGUI(float deltaTime);

	float getTextWidth(Text *text);
	void createLines(Text *text, float windowWidth, vector<string> &linesVector);
	string cutStringFromLeft(Text *text, float windowWidth);
};

class ChatGUI {
public:
	ChatGUI(ClientGame *clientObj);
	ClientGame *clientObject;

	Sprite chatSprite;
	Sprite inputFieldChatSprite;
	Font chatFont;
	Text chatLine;

	string displayedMessage;

	float chatXOffset = 30.0;
	float chatYOffset = 30.0;
	float chatXSize = windowXSize * 0.33;
	float chatYSize = windowYSize * 0.2;
	float chatXPos = chatXOffset;
	float chatYPos = windowYSize - chatYSize - chatYOffset;

	int fontSize = 12;
	int lineDiffX = 5;
	int lineDiffY = 3;
	int amountOfLines = (int)chatYSize / (fontSize + lineDiffY) - 1;

	void createChatBox();
	void showChatBox();
	void showChatText();
	void showClientMessage(float deltaTime);

	int cursorXLeftDiff = 2;
	float cursorTime = 0.0;
	float tickTime = 0.5;
	unsigned int cursorGUIPos = 0;
	unsigned int lastCursorPos = 0;
	int lastTextSize = 0;
	void moveCursorGUIRight();
	void moveCursorGUILeft();
	bool cursorTimer(float deltaTime);
};




