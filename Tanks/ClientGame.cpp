#include "ClientGame.h"

ClientGame::ClientGame(string ipString, string clientNick) : id(-1), side(-1) {
    network = new Client(ipString);
	chatObject = new Chat(this);
	guiObject = new GUI(this);
	nick = clientNick;
}

void ClientGame::update(map<int, Player> &playerMap) {
    Packet packet;
	IDPacket *idPacket;
	PlayerPacket *playerPacket;
	BulletPacket *bulletPacket;
	ChatPacket *chatPacket;

	Sprite bullet;

    int data_length = network->receivePackets(network_data);

    if (data_length <= 0) {
        // nie otrzymano danych
        return;
    }

    unsigned int i = 0;
    while (i < (unsigned int)data_length) {
        
		// Pierwszy bit okresla typ pakietu
		packet.deserialize(&(network_data[i]));

        switch (packet.packet_type) {
		case ID_PACKET:
			if (id == -1) {
				idPacket = new IDPacket();
				idPacket->deserialize(&(network_data[i]));
				id = idPacket->id;
				side = idPacket->side;
				// Uzupelnienie wlasnych danych o info otrzymane z serwera
				playerMap[id].id = id;
				playerMap[id].side = side;
				cout << "Klient otrzymal od serwera ID o nr: " << id << endl;
			}
			i += sizeof(IDPacket);
			break;
		case PLAYER_PACKET:
			playerPacket = new PlayerPacket();
			playerPacket->deserialize(&(network_data[i]));
			if (playerPacket->id != id) {
				playerMap[playerPacket->id].id = playerPacket->id;
				playerMap[playerPacket->id].playerSprite.setPosition(playerPacket->x, playerPacket->y);
				playerMap[playerPacket->id].playerSprite.setRotation(playerPacket->angle);
				playerMap[playerPacket->id].side = playerPacket->side;
				playerMap[playerPacket->id].isDead = playerPacket->isDead;
			}
			i += sizeof(PlayerPacket);
			//cout << "Klient otrzymal pakiet gracza od serwera." << playerPacket.id << endl;
			break;
		case BULLET_PACKET:
			bulletPacket = new BulletPacket();
			bulletPacket->deserialize(&(network_data[i]));
			if (bulletPacket->id != id) {
				bullet.setTexture(playerMap[bulletPacket->id].bulletTextureVector[0]);
				bullet.setOrigin((float)playerMap[bulletPacket->id].bulletTextureVector[0].getSize().x / 2, (float)playerMap[bulletPacket->id].bulletTextureVector[0].getSize().y / 2);
				bullet.setPosition(bulletPacket->x, bulletPacket->y);
				bullet.setRotation(bulletPacket->angle);
				playerMap[bulletPacket->id].bulletSpriteVector.push_back(bullet);
				//cout << "Klient otrzymal pakiet pocisku od serwera " << bulletPacket.id << endl;
			}
			i += sizeof(BulletPacket);
			break;
		case CHAT_PACKET:
			chatPacket = new ChatPacket();
			chatPacket->deserialize(&(network_data[i]));
			chatObject->showMessage(chatPacket);
			i += sizeof(ChatPacket);
			break;
        default:
			cout << "Klient - Bledny typ odebranego pakietu." << endl;
			break;
        }
    }
}

void ClientGame::clientLoop(map<int, Player> &playerMap) {
    Time time = milliseconds(10);
	while (true) {
		// Aktualizownie stanu klienta
		update(playerMap);

		// Wysylanie danych o kliencie na serwer
		if (id != -1) {
			sendPacket(PlayerPacket(playerMap[id]));
		}
		sleep(time);
    }
	terminate();
}

void ClientGame::eventHandler(map<int, Player> &playerMap) {
	Event EventObject;
	if (!chatObject->ifTyping()) {
		while (window.pollEvent(EventObject)) {
			switch (EventObject.type) {
			case Event::Closed:
				window.close();
				break;
			case Event::KeyPressed:
				switch (EventObject.key.code) {
				case Keyboard::Space:
					sendPacket(BulletPacket(playerMap[id].shot(), id));
					#pragma WARNING("funkcja dla sendPacket bullet?")
					break;
				case Keyboard::Up:
					playerMap[id].keyPressed(upD);
					break;
				case Keyboard::Down:
					playerMap[id].keyPressed(downD);
					break;
				case Keyboard::Left:
					playerMap[id].keyPressed(leftD);
					break;
				case Keyboard::Right:
					playerMap[id].keyPressed(rightD);
					break;
				case Keyboard::Escape:
					// TODO - okno dialogowe z pytaniem o zamkniecie gry
					//window.close();
					break;
				case Keyboard::Return:
					chatObject->chatTypingOn();
					break;
				default:
					break;
				}
				break;
			case Event::KeyReleased:
				switch (EventObject.key.code) {
				case Keyboard::Up:
					playerMap[id].keyReleased(upD);
					break;
				case Keyboard::Down:
					playerMap[id].keyReleased(downD);
					break;
				case Keyboard::Left:
					playerMap[id].keyReleased(leftD);
					break;
				case Keyboard::Right:
					playerMap[id].keyReleased(rightD);
					break;
				default:
					break;
				}
				break;
			default:
				break;
			}
		}
	} else {
		while (window.pollEvent(EventObject)) {
			switch (EventObject.type) {
			case Event::Closed:
				window.close();
				break;
			case Event::KeyPressed:
				switch (EventObject.key.code) {
				case Keyboard::Up:
					chatObject->retypeLastMessage();
					break;
				case Keyboard::Left:
					chatObject->moveCursorLeft();
					break;
				case Keyboard::Right:
					chatObject->moveCursorRight();
					break;
				case Keyboard::Escape:
					chatObject->chatTypingOff();
					break;
				case Keyboard::Return:
					chatObject->sendMessage();
					chatObject->chatTypingOff();
					break;
				case Keyboard::BackSpace:
					chatObject->backspaceMessage(EventObject);
					break;
				case Keyboard::Delete:
					chatObject->deleteMessage(EventObject);
					break;
				default:
					break;
				}
				break;
			case Event::TextEntered:
				chatObject->writeMessage(EventObject);
				break;
			default:
				break;
			}
		}
	}
}