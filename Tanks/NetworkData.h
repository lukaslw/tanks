#pragma once
#include <string.h>
#include "Common.h"
#include "Player.h"

#define MAX_PACKET_SIZE 1000000

// Rodzaje pakietow
enum PacketTypes {
	INIT_PACKET = 0,
	PLAYER_PACKET = 1,
	ID_PACKET = 2,
	BULLET_PACKET = 3,
	KILL_PACKET = 4,
	CHAT_PACKET = 5
};


// Ogolna struktura pakietu
// zawiera typ pakietu jako glowny parametr
struct Packet {
    unsigned int packet_type; // typ pakietu

    void serialize(char *data) {
		memcpy(data, this, sizeof(this));
    }

	void deserialize(char *data) {
		memcpy(this, data, sizeof(this));
    }
};

// Struktura po ktorej przeciazane sa inne pakiety
// udostepnia metody serializacji i deserializacji pakietu
template <typename ChildPacket>
struct PacketParent : public Packet {
	void serialize(char *data) {
		memcpy(data, this, sizeof(ChildPacket));
	}

	void deserialize(char *data) {
		memcpy(this, data, sizeof(ChildPacket));
	}
};

// P. inicjujacy
struct InitPacket : public PacketParent<InitPacket> {
	InitPacket() {
		packet_type = INIT_PACKET;
	}
	InitPacket(unsigned int clientID, string playerNick) {
		packet_type = INIT_PACKET;
		id = clientID;
		nick = playerNick;
	}
	unsigned int id;
	string nick;
};

// P. nadajacy id klientowi
struct IDPacket : public PacketParent<IDPacket> {
	IDPacket() {
		packet_type = ID_PACKET;
	}
	IDPacket(unsigned int clientID, unsigned int clientSide) {
		packet_type = ID_PACKET;
		id = clientID;
		side = clientSide;
	}
	unsigned int id; // id klienta
	unsigned int side; // strona po ktorej walczyc bedzie klient
};

// P. gracza
class PlayerPacket : public PacketParent<PlayerPacket> {
public:
	PlayerPacket() {
		packet_type = PLAYER_PACKET;
	}
	PlayerPacket(Player &player) {
		packet_type = PLAYER_PACKET;
		id = player.id;
		side = player.side;
		x = player.playerSprite.getPosition().x;
		y = player.playerSprite.getPosition().y;
		angle = player.playerSprite.getRotation();
		isDead = player.isDead;
	}
	int id; // ktory player
	int side; // ktora strona
	float x; // polozenie X
	float y; // polozenie Y
	float angle; // kat nachylenia
	bool isDead; //nie zyje
};

// P. pocisku
struct BulletPacket : public PacketParent<BulletPacket> {
	BulletPacket() {
		packet_type = BULLET_PACKET;
	}
	BulletPacket(Sprite &bullet, int playerId) {
		packet_type = BULLET_PACKET;
		id = playerId;
		x = bullet.getPosition().x;
		y = bullet.getPosition().y;
		angle = bullet.getRotation();
	}
	int id; // ktory player
	float x; // polozenie X
	float y; // polozenie Y
	float angle; // kat nachylenia
};

// P. zabicia gracza
struct KillPacket : public PacketParent<KillPacket> {
	KillPacket() {
		packet_type = KILL_PACKET;
	}
	KillPacket(int victimPlayerID, int killerPlayerID) {
		packet_type = KILL_PACKET;
		victimID = victimPlayerID;
		killerID = killerPlayerID;
	}
	int victimID;
	int killerID;
};

// P. wiadomosci chatu
struct ChatPacket : public PacketParent <ChatPacket> {
	ChatPacket() {
		packet_type = CHAT_PACKET;
	}
	ChatPacket(string message, string clientNick) {
		packet_type = CHAT_PACKET;
		strncpy_s(messageText, message.c_str(), sizeof(messageText));
		messageText[sizeof(messageText) - 1] = 0;

		strncpy_s(nick, clientNick.c_str(), sizeof(nick));
		nick[sizeof(nick) - 1] = 0;
	}
	char messageText[200];
	char nick[20];
};